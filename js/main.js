require.config({
    waitSeconds: 30,
    baseUrl: '/js/',
    urlArgs: 'ver=1.1.9',

    paths: {
        'jquery': 'bower_components/jquery/jquery.min',
        'jquery.serializeobject': 'vendors/jquery.serializeobject',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'bootstrap-multiselect': 'bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect',
        'bootstrap-datetimepicker': 'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min',
        'underscore': 'bower_components/underscore/underscore-min',
        'backbone': 'bower_components/backbone/backbone-min',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'backbone-filtered-collection': 'bower_components/backbone-filtered-collection/backbone-filtered-collection',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
        'ndvi': 'vendors/ndvi',
        'pallete': 'vendors/pallete',
        'sprintfjs': 'bower_components/sprintf/dist/sprintf.min',
        'moment': 'bower_components/moment/min/moment-with-locales.min',
        'js-cookie': 'bower_components/js-cookie/src/js.cookie',
        'leaflet': 'bower_components/leaflet/dist/leaflet',
        'Leaflet.MultiOptionsPolyline': 'bower_components/Leaflet.MultiOptionsPolyline/Leaflet.MultiOptionsPolyline.min',
        'leaflet.markercluster': 'bower_components/leaflet.markercluster/dist/leaflet.markercluster-src',
        'leaflet-groupedlayercontrol': 'bower_components/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min',
        'leaflet-rotatedmarker': 'bower_components/leaflet-rotatedmarker/leaflet.rotatedMarker',
        'leaflet-openweathermap': 'bower_components/leaflet-openweathermap/leaflet-openweathermap',
        'leaflet-sidebar': 'vendors/leaflet/leaflet-sidebar'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'leaflet': {
            exports: 'L'
        },
        'Leaflet.MultiOptionsPolyline': {
            deps: [ 'leaflet' ]
        },
        'leaflet.markercluster': {
            deps: [ 'leaflet' ]
        },
        'leaflet-groupedlayercontrol': {
            deps: [ 'leaflet' ]
        },
        'leaflet-sidebar': {
            deps: [ 'leaflet' ]
        },
        'leaflet-rotatedmarker': {
            deps: [ 'leaflet' ]
        },
        'leaflet-openweathermap': {
            deps: [ 'leaflet' ]
        },
        'moment': {
            deps: [],
        },
        'jquery.serializeobject': {
	    	deps: [ 'jquery' ],
	    	exports: '$.fn.serializeObject'
		},
        'highcharts': {
            deps: ['jquery']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap-multiselect': {
            deps: ['jquery', 'bootstrap'],
        },
        'bootstrap-datetimepicker': {
            deps: ['jquery', 'bootstrap', 'moment'],
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([

    'jquery',
    'app',

    'models/account',

    'bootstrap',
    'leaflet',

    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
    'css!vendors/leaflet/leaflet-sidebar.min.css',
    'css!bower_components/leaflet/dist/leaflet.css',
    'css!stylesheets/common_style',

    'bootstrap'

 ], function($, app, Account) {

    $(function() {

        app.account = new Account();
		app.account.fetch({
			success: function() {

                app.account.set('isAdmin', app.account.hasGroup('admins'));
                
				app.start({});
			}
		});
    });
});
