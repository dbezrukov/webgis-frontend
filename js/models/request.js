define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/api/flightrequests',

        defaults: function() {
            return {
                name: '',
                description: '',
                placeName: '',
                date: '',
                startUtc: '',
                duration: '',
                zoneCenter: '',
                zoneRadius: 10,
                hgtAMSL: 100,
                hgtAGL: 100,
                company: '',
                fcoName: '',
                fcoPhone: '',
                pilotName: '',
                pilotPhone: ''
            }
        }
    });
});
