define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: '/api/target-info',

        defaults: function() {
            return {
            }
        }
    });
});
