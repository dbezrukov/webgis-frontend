define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'Id',

        defaults: function() {
            return {
            }
        },

        initialize: function(){
            var self = this;
        },
    });
});
