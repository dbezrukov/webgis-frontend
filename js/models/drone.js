define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/api/drones',

        defaults: function() {
            
            return {
                model: '',
                type: '',
                serial: '',
                weight: 1.0,
                photo: 'drones/default'
            }
        }
    });
});
