define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/api/features',
        
        defaults: function() {
            return {
                id: '',
                type: 'Feature',
                geometry: {},
                properties: {}
            }
        }
    });
});
