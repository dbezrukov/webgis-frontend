define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: '/api/targets',
        
        defaults: function() {
            return {
                valid: false,
                updated: 0,
                type: 'uav',
                
                position: {
                    lat: 0,
                    lon: 0,
                    utc: 0,
                    hgt: 0,
                    altbaro: 0,
                    pch: 0,
                    roll: 0,
                    yaw: 0,
                    hdg: 0,
                    speed: 0,
                    mode: '',
                    source: ''
                }
            }
        },

        startMonitoring: function() {
            var self = this;

            self.timer = setInterval(function() {
                self.fetch();
            }, 1000);

            self.fetch();
        },

        stopMonitoring: function() {
            var self = this;

            if (self.timer) {
                clearInterval(self.timer);
            }
        }
    });
});
