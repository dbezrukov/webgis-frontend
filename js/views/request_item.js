define([
    'underscore',
    'marionette',
    
    'tpl!templates/request_item.tmpl',
    
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'request-item'
    });
});