/* Личный кабинет пользователя */

define([
	'underscore',
    'sprintfjs',
    'marionette',
    
    'views/map_widget',
    'views/sidebar_widget',
    
    'collections/flights',
    'collections/targets',

    'layers/flights',
    'layers/targets',

    'tpl!templates/main_pane.tmpl',
    'css!stylesheets/main_pane',

    'leaflet-sidebar'
    
], function(_, sprintf, Marionette, 
	MapWidget, 
    SidebarWidget,
	
    Flights, 
    Targets,
	LayerFlights, 
    LayerTargets,
	template) {

   return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'main-pane',

        ui: {
        	'sidebarWidgetWrapper': '.sidebar-widget-wrapper',
        	'mapWidgetWrapper': '.map-widget-wrapper'
        },

        initialize: function() {
            var self = this;

            self.targets = new Targets;
        },

        onRender: function() {
        	var self = this;

        	self.initMap();
        	self.initSidebar();

            app.vent.on('source:trigger', self.triggerSource.bind(self));
        },

        initMap: function() {
        	var self = this;

			var layerFlights = new LayerFlights({
				collection: new Flights
			});

			var layerTargets = new LayerTargets({
				collection: self.targets,
				icons: {
					uav: new L.icon({
    					iconUrl: 'img/targets/uav.svg',
						iconSize: [36, 36],
						iconAnchor: [18, 18],
						popupAnchor: [0, -25],
						shadowUrl: null
					}),
                    boat: new L.icon({
                        iconUrl: 'img/targets/boat.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    }),
                    man: new L.icon({
                        iconUrl: 'img/targets/man.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    }),
                    vehicle: new L.icon({
                        iconUrl: 'img/targets/vehicle.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    }),
                    copter: new L.icon({
                        iconUrl: 'img/targets/copter.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    }),
                    heli: new L.icon({
                        iconUrl: 'img/targets/heli.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    })
				}
			});

            var optionsOWM = { showLegend: false, opacity: 0.5, appId: 'e220da0ef978e0dea95beac16028ca30' }

            self.mapWidget = new MapWidget({
            	model: self.model,
                userLayers: {
                    'Беспилотные аппараты': { layer: layerTargets, show: true },
                    'Гражданская авиация': { layer: layerFlights, show: false }
                }
        	});

        	self.ui.mapWidgetWrapper.append(self.mapWidget.render().el);
        },

        initSidebar: function() {
        	var self = this;

            var sidebarWidget = new SidebarWidget({
                collection: self.targets,
                mapWidget: self.mapWidget
            });

            self.ui.sidebarWidgetWrapper.append(sidebarWidget.render().el);
        },

        onShow: function() {
            var self = this;

            self.mapWidget.onShow();

            var sidebar = L.control.sidebar('sidebar').addTo(self.mapWidget.map);

            self.targets.on('selected', function(model, value) {
                
                var windowWidth = self.ui.sidebarWidgetWrapper.css('width');
                var sidebarWidth = self.ui.sidebarWidgetWrapper.children().css('width');
                
                if (parseInt(sidebarWidth) > parseInt(windowWidth) / 2) {
                    sidebar.close();
                }
            });
        },

        triggerSource: function(sourceModel) {
            var self = this;

            var targetId = sourceModel.get('targetId');

            console.log('Triggering source for target: ' + targetId);

            var target = self.targets.get(targetId);

            if (target) {
                var activeSource = target.get('source');
                if (activeSource && (activeSource.get('name') === sourceModel.get('name'))) {
                   target.unset('source');
                } else {
                   target.set('source', sourceModel); 
                }
            }
        }
    });
});
