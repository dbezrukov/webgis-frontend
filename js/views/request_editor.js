define([
    'underscore',
    'marionette',

    'sprintfjs',
    'collections/drones',
    'views/map_widget',

    'tpl!templates/request_editor.tmpl',

    'bootstrap-multiselect',
    'jquery.serializeobject'

], function(_, Marionette, 
    sprintf,
    Drones,
    MapWidget, 
    template) {

    function multiselectData(entities, defaultEntities) {
        var data = [];

        entities.forEach(function(entity) {
            data.push({ 
                label: entity.get('model'), 
                value: entity.get('_id'),
                selected: _.contains(defaultEntities, entity.get('_id'))
            });
        });

        return data;
    }

    function convertToDMS(lat, lng) {

        function toDMS(n) {
            n = Math.abs(n);

            // Get the degrees
            var d = Math.floor(n % 360);
            n = n - d;
            n *= 60;
            
            // Get the minutes
            var m = Math.floor(n);
            n = n - m;
            n *= 60;
                
            // Get the seconds
            var s = Math.floor(n);

            return sprintf.sprintf('%02d%02d%02d', d, m, s);
        }

        var dirLat = lat > 0 ? 'С' : 'Ю';
        var dirLng = lng > 0 ? 'В' : 'З';
      
        return toDMS(lat) + dirLat + toDMS(lng) + dirLng;
    }

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'request-editor',

        ui: {
            'alert': '.alert',
            'select' : '.multiselect',
            'zoneCenter': 'input[name="zoneCenter"]',
            'mapWidgetWrapper': '.map-widget-wrapper'
        },

        events: {
            'click a.save': 'saveRequest',
            'click a.cancel': 'cancelRequest',
            'click a.delete': 'deleteRequest'
        },

        templateHelpers: {
            editable: function() {
                return app.account.get('_id');
            },
            requestName: function() {
                if (this.name) {
                    return this.name;
                } else {
                    return 'Новая заявка';
                }
            }
        },

        onRender: function() {
            this.initMap();
            this.initEquipment();
        },

        initMap: function() {
            this.mapWidget = new MapWidget({
                model: this.model,
                centerChanged: center => {
                    this.ui.zoneCenter.val(convertToDMS(center.lat, center.lng));
                }
            });

            this.ui.mapWidgetWrapper.append(this.mapWidget.render().el);
        },

        initEquipment: function() {

            this.ui.select.multiselect({
                nonSelectedText: 'Выберите оборудование',
                allSelectedText: 'Все БПЛА'
            }); 

            var drones = new Drones;

            drones.fetch({
                success: () => {
                    this.ui.select.multiselect('dataprovider', multiselectData(drones, this.model.get('drones')));
                }
            });
        },

        onShow: function() {
            this.mapWidget.onShow();
        },

        saveRequest: function() {
            var data = $(this.el).find('form').serializeObject();

            var drones = this.ui.select.val();
            if (!drones) {
                this.ui.alert.text('Укажите оборудование');
                this.ui.alert.show();
                return;
            }

            this.ui.alert.hide();

            data.drones = drones.map(droneId => {
                return {
                    _id: droneId
                };
            });

            var options = {
                type: 'POST',
                wait: true,
                success: (model, response) => {
                    app.navigate('/requests', {
                        trigger: true
                    });
                },
                error: (model, response) => {
                    this.ui.alert.text(response.responseJSON ?
                        response.responseJSON.error :
                        response.responseText);

                    this.ui.alert.show();
                    this.ui.alert.focus();
                }
            }

            this.model.save(data, options);
        },

        cancelRequest: function() {
            var url = this.model.isNew()
                ? '/requests'
                : '/requests/' + this.model.get('_id');

            app.navigate(url, { trigger: true });
        },

        deleteRequest: function() {
            var confirmation = confirm('Удалить эту заявку?');

            if (confirmation !== true) {
                return;
            }

            this.model.destroy();

            app.navigate('/requests', {
                trigger: true
            });
        }
    });
});