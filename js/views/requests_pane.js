define([
    'jquery',
    'underscore',
    'marionette',
    'views/request_item',
    
    'tpl!templates/requests_pane.tmpl'
    
], function($, _, Marionette, ItemView, templatePanel) {

    return Marionette.CompositeView.extend({
        template: templatePanel,
        itemView: ItemView,
        itemViewContainer: '.requests',
        tagName: 'div',
        className: "requests-pane",

        ui: {
            filter: 'input[name="filter"]'
        },

        events: {
            'keyup input[name="filter"]': 'filterChanged',
            'click .remove': 'filterRemoved'
        },

        templateHelpers: {
            editable: function() {
                return app.account.get('_id');
            }
        },

        filterChanged: function() {
            var name = this.ui.filter.val();

            this.collection.removeFilter('name');

            if (!name) {
                return;
            }

            this.collection.filterBy('name', function(model) {
                return model.get('name').indexOf(name) >= 0 
                    || model.get('placeName').toLowerCase().indexOf(name.toLowerCase()) >= 0;
            });
        },

        filterRemoved() {
            this.ui.filter.val('');
            this.filterChanged();
        }
    });
});
