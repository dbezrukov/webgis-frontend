define([
    'underscore',
    'marionette',
    'tpl!templates/drone_editor.tmpl',

    'jquery.serializeobject'

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'drone-editor',

        ui: {
            'alert': '.alert'
        },

        events: {
            'click a.save': 'saveDrone',
            'click a.cancel': 'cancelDrone',
            'click a.delete': 'deleteDrone',
            'click img.upload': 'uploadPhoto'
        },

        modelEvents: {
            'change': 'render'
        },

        templateHelpers: {
            editable: function() {
                return app.account.get('_id');
            },
            droneName: function() {
                if (this.model) {
                    return this.model;
                } else {
                    return 'Новый БПЛА';
                }
            },
            dronePhoto: function() {
                return app.account.get('photostorage') + this.photo;
            }
        },

        saveDrone: function() {
            var data = $(this.el).find('form').serializeObject();
            
            this.ui.alert.hide();

            var options = {
                type: 'POST',
                wait: true,
                success: (model, response) => {
                    app.navigate('/drones', {
                        trigger: true
                    });
                },
                error: (model, response) => {
                    this.ui.alert.text(response.responseJSON ?
                        response.responseJSON.error :
                        response.responseText);

                    this.ui.alert.show();
                    this.ui.alert.focus();
                }
            }

            this.model.save(data, options);
        },

        cancelDrone: function() {
            var url = this.model.isNew()
                ? '/drones'
                : '/drones/' + this.model.get('_id');

            app.navigate(url, { trigger: true });
        },

        deleteDrone: function() {
            var confirmation = confirm('Удалить этот БПЛА?');

            if (confirmation !== true) {
                return;
            }

            this.model.destroy();

            app.navigate('/drones', {
                trigger: true
            });
        },

        uploadPhoto: function() {
            var self = this;

            if (!self.templateHelpers.editable()) {
                return;
            }

            //http://cloudinary.com/documentation/upload_widget
            cloudinary.openUploadWidget({
                cloud_name: app.account.get('cloudinary').cloud_name,
                    api_key: app.account.get('cloudinary').api_key,
                    upload_preset: 'drones',
                text: {
                    'powered_by_cloudinary': 'Powered by Cloudinary - Image management in the cloud',
                    'sources.local.title': 'Файлы',
                    'sources.local.drop_file': 'Перетащите файл сюда',
                    'sources.local.drop_files': 'Перетащите файлы сюда',
                    'sources.local.drop_or': 'Или',
                    'sources.local.select_file': 'Выбрать файл',
                    'sources.local.select_files': 'Выбрать файлы',
                    'sources.url.title': 'Web адрес',
                    'sources.url.note': 'Ссылка на файл:',
                    'sources.url.upload': 'Загрузить',
                    'sources.url.error': 'Пожалуйста укажите верную ссылку',
                    'sources.camera.title': 'Камера',
                    'sources.camera.note': 'Убедитесь, что доступ к камере разрешен и нажмите "Сделать снимок":',
                    'sources.camera.capture': 'Сделать снимок',
                    'progress.uploading': 'Загрузка...',
                    'progress.upload_cropped': 'Загрузить',
                    'progress.processing': 'Обработка...',
                    'progress.retry_upload': 'Попробовать еще раз',
                    'progress.use_succeeded': 'OK',
                    'progress.failed_note': 'Часть изображений не была загружена'
                }
            },
            function(error, result) {
                
                if (error) {
                    console.log('file uploading error: ' + error);
                    return;
                }

                if (!result) {
                    return;
                }

                self.ui.alert.hide();
            
                var data = $(self.el).find('form').serializeObject();
                data.photo = result[0].public_id;

                self.model.set(data);
            });
        }
    });
});