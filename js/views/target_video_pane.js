define([
    'underscore',
    'marionette',

    'views/source_item',

    'collections/sources',    

    'tpl!templates/target_video_pane.tmpl',
    'css!stylesheets/target_video_pane.css'

], function(_, Marionette, 
    ItemView, 
    Sources,
    template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.sources',
        tagName: 'div',
        className: 'target-video-pane unselectable',

        ui: {
            videoContainer: '.video-container',
            videoImage: '.video-image',
            videoRotate: '.video-rotate'
        },
        
        events: {
            'click .video-rotate': 'onRotateImage'
        },
        
        modelEvents: {
            'change:source': 'sourceChanged'
        },

        collectionEvents: {
            'remove': 'sourceRemoved'
        },

        templateHelpers: {
        },

        initialize: function() {
            var self = this;

            self.collection = new Sources({
                targetId: self.model.get('id'),
                name: '' // unknown so far
            })

            self.desirableSource = self.model.get('source');
        },

        onRender: function() {
            var self = this;

            self.videoRotated = false;
        },

        onShow: function() {
            var self = this;

            console.log(self.collection.length);

            self.fetchSources();
            self.fetchTimer = setInterval(self.fetchSources.bind(self), 5000);
        },

        onClose: function(){
            var self = this;

            clearInterval(self.fetchTimer);
            
            self.collection.reset();
            if (self.model.get('source')) {
                self.model.unset('source');
            }
        },

        onRotateImage: function() {
            var self = this;

            self.videoRotated = !self.videoRotated;
            self.ui.videoImage.attr('rotated', self.videoRotated);
        },

        fetchSources: function() {
            var self = this;
            self.collection.fetch({
                success: function() {

                    if (self.desirableSource) {
                        
                        var source = self.collection.findWhere({
                            name: self.desirableSource.get('name')
                        })

                        if (source) {
                            self.model.set('source', source);
                        }

                        delete self.desirableSource;
                    }
                }
            });
        },

        sourceRemoved: function(removedSource) {
            var self = this;

            if (self.model.get('source') === removedSource) {
                self.model.unset('source');
            }
        },

        sourceChanged: function(targetModel, sourceModel) {
            var self = this;

            if (sourceModel) {
                $(self.ui.videoImage).attr('src', sourceModel.get('url'));
                $(self.ui.videoContainer).show();
            } else {
                $(this.ui.videoImage).attr('src', '');
                $(this.ui.videoContainer).hide();
            }
        }
    });
});
