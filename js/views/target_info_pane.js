define([
    'underscore',
    'marionette',
    'tpl!templates/target_info_pane.tmpl',

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'target-info-pane unselectable',

        ui: {
        },
        
        events: {
        },
        
        modelEvents: {
            'change': 'render'
        },

        templateHelpers: {
            helperData: function() {
                var pairs = _.pairs(this.data);

                var string = '';
                _.each(this.data, function(val, key) {
                    string += key + ': <b>' + val + '</b><br>';
                });

                return string;
            }
        },

        onRender: function() {
            var self = this;
        }
    });
});
