define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/flight_request_modal.tmpl',
    'css!bower_components/font-awesome/css/font-awesome.min.css',
    'jquery.serializeobject'

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padded-wrapper col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3',

        ui: {
        },

        events: {
            'click #btnSubmit': 'submitChanges',
            'keypress': 'keyPressed',
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        keyPressed: function(e) {
            var self = this;

            if (e.keyCode == 13) {
                self.submitChanges.call(self);
            }
        },

        onRender: function() {
        },

        onShow: function(){
        },

        submitChanges: function() {
            this.close();
        }
    });
});
