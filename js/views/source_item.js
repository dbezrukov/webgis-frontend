define([
    'marionette',
    'tpl!templates/source_item.tmpl',
    'css!stylesheets/source_item'
    
], function(Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',

        ui: {
            sourceIcon: '.source-icon'
        },

        events: {
            'click .source-icon': 'onSourceClicked'
        },
  
        initialize: function(){
        },

        onRender: function() {
            var self = this;

            var sourceName = self.model.get('name');
            if (!sourceName) {
                return;
            }

            var iconClass = 'glyphicon-facetime-video';

            if (sourceName.indexOf('photo') >= 0) {
                iconClass = 'glyphicon-camera';
            }
            else if (sourceName.indexOf('wv') >= 0) {
                iconClass = 'glyphicon-fire';
            }

            $(self.ui.sourceIcon).addClass(iconClass);
            $(self.ui.sourceIcon).show();
        },

        onSourceClicked: function() {
            var self = this;
            app.vent.trigger('source:trigger', self.model);
        }

    });
});
