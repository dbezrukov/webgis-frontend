define([
    'underscore',
    'marionette',

    'tpl!templates/drone_pane.tmpl',
        
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'drone-pane',

        templateHelpers: {
            editable: function() {
                return app.account.get('_id');
            }
        }
    });
});
