define([
    'marionette',

    'views/target_pane',
    'views/target_telemetry_pane',
    'views/target_video_pane',
    
    'tpl!templates/target_item.tmpl',

], function(Marionette, 
    TargetPane, 
    TargetTelemetryPane, 
    TargetVideoPane, 
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',
        
        ui: {
            'triggerTelemetryIcon': '.trigger-telemetry',
            'layoutWrapper': '.layout-wrapper'
        },

        events: {
            'click .select-target': 'selectTarget',
            'click .trigger-telemetry': 'triggerTelemetry'
        },

        templateHelpers: {
            helperIcon: function() {
                return 'img/targets/' + this.type + '.svg';
            },
            helperSource: function() {
                return (this.position.source === 'emulator')
                    ? ' (эмулятор)'
                    : '';
            }
        },

        onRender: function() {
            var self = this;

            var TargetPaneLayout = Marionette.Layout.extend({
                template: _.template('\
                    <div id="telemetry"></div>\
                    <div id="video" style="width: 160px; margin-top: 10px; margin-bottom: 10px; "></div>\
                '),

                /* <a class="target-link" style="cursor: pointer;">открыть окно цели</a>\ */

                regions: {
                    telemetry: "#telemetry",
                    video: "#video"
                },

                events: {
                    'click .target-link': 'onTargetLink'
                },

                /*
                onTargetLink: function() {
                    var self = this;

                    app.layout.modal.show(new TargetPane({
                        model: self.model
                    }));
                }
                */
            });

            self.layout = new TargetPaneLayout({
                model: self.model // target model
            });
        }, 
		
        selectTarget: function() {
            this.model.trigger('selected', this.model);
        },

        triggerTelemetry: function() {
            var self = this;

            if (self.ui.layoutWrapper.html()) {
                
                self.ui.triggerTelemetryIcon.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                self.layout.close();

            } else {
                self.ui.triggerTelemetryIcon.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');

                self.ui.layoutWrapper.html(self.layout.render().el);

                self.layout.telemetry.show(new TargetTelemetryPane({
                    model: self.model // target model
                }));

                /*
                self.layout.video.show(new TargetVideoPane({
                    model: self.model // target model
                }));
                */
            }
        }
    });
});