define([
    'underscore',
    'marionette',

    'views/target_telemetry_pane',
    'views/target_video_pane',

    'tpl!templates/target_pane.tmpl',
    'css!stylesheets/target_pane',

], function(_, Marionette,

    TargetTelemetryPane,
    TargetVideoPane,

    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'target-pane',

        ui: {
            layoutWrapper: '.layout-wrapper'
        },

        templateHelpers: {
        },

        events: {
            'click .map-link': 'onMapLink'
        },

        modelEvents: {
        },

        initialize: function() {
            var self = this;
        },

        onRender: function() {
            var self = this;

            var TargetPaneLayout = Marionette.Layout.extend({
                className: 'row',
                template: _.template('\
                    <div id="telemetry" class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom: 20px;"></div>\
                    <div id="video"     class="col-xs-12 col-sm-10 col-md-8 col-lg-7"></div>\
                    <div id="info"></div>\
                '),
                regions: {
                    telemetry: "#telemetry",
                    video: "#video",
                    info: "#info"
                }
            });

            var layout = new TargetPaneLayout({
                model: self.model // target model
            });
            
            layout.render();

            layout.telemetry.show(new TargetTelemetryPane({
                model: self.model // target model
            }));

            layout.video.show(new TargetVideoPane({
                model: self.model // target model
            }));

            self.ui.layoutWrapper.html(layout.el);

            self.model.startMonitoring();
        }, 

        onMapLink: function(e) {
            var self = this;
            self.close();
        }
    })
});
