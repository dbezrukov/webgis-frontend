define([
    'underscore',
    'marionette',
    
    'layers/zone_features',
    'layers/point_features',

    'ndvi',
    'js-cookie',
    'tpl!templates/map_widget.tmpl',
    'css!stylesheets/map_widget',
    'leaflet-groupedlayercontrol',
    'leaflet-rotatedmarker',
    'leaflet-openweathermap',
    'css!bower_components/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min.css',
    'css!bower_components/leaflet-openweathermap/leaflet-openweathermap.css'

], function(_, 
    Marionette, 

    ZoneFeatures,
    PointFeatures,

    Ndvi, Cookies, template) {

    const zones = {
        lz: {
            type: 'Зона ограничения полётов',
            style: function(feature) {
                return {
                    fillColor: 'transparent',
                    color: '#f00',
                    weight: 2,
                    opacity: 0.5,
                    fillOpacity: 0
                }
            }
        },
        ez: {
            type: 'Запретная зона',
            style: function(feature) {
                return {
                    fillColor: '#f00',
                    color: '#f00',
                    weight: 1,
                    opacity: 1,
                    fillOpacity: 0.15
                }
            }
        },
        dz: {
            type: 'Опасная зона',
            style: function(feature) {
                return {
                    fillColor: '#f00',
                    color: '#f00',
                    weight: 1,
                    opacity: 1,
                    fillOpacity: 0.6
                }
            }
        },
        ard: {
            type: 'Аэродром',
            style: function(feature) {}
        },
        vl: {
            type: 'Воздушные линии',
            style: function(feature) {}
        },
        mvl: {
            type: 'Местные воздушные линии',
            style: function(feature) {}
        }
    }

    function zone(name) {
        const createPopup = function(feature, layer) {
            if (feature.properties) {
                if (feature.properties && feature.properties.description) {
                    const type = zones[name].type;
                    const header = `${type}: <b>${feature.properties.name}</b><br><br>`;
                    layer.bindPopup(header + feature.properties.description.replace(/\n/g, '<br />'));
                }
            }
        }

        return L.geoJson(null, {
            style: zones[name].style,
            onEachFeature: createPopup
        }).on('add', e => {
            $.getJSON(`/vp/${name}.geojson`, data => { 
                try {
                    e.target.addData(data);
                } catch (err) {

                }
            })
        });
    }

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'widget map-widget',

        ui: {
            'map': '.map',
        },

        initialize: function(options) {
            options || (options = {});
            this.options = options;

            var boundsStr = Cookies.get('map-bounds');
            if (boundsStr) {
                var bounds = JSON.parse(boundsStr);
                var corner1 = L.latLng(bounds._northEast.lat, bounds._northEast.lng);
                var corner2 = L.latLng(bounds._southWest.lat, bounds._southWest.lng);
                this.initialBounds = L.latLngBounds(corner1, corner2);
            }
        },

        onRender: function() {
            this.initMap();
        },

        initMap: function() {
            var startLevel = 12;
            var centerLon = 37.620568;
            var centerLat = 55.754056;

            var redraw = !!this.map;

            this.baseLayers = {
                'Google': new L.TileLayer('https://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga'),
                'OSM': new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
            };

            this.map = L.map(this.ui.map[0], {
                center: new L.LatLng(centerLat, centerLon),
                zoom: startLevel,
                minZoom: 0,
                maxZoom: 18,
                crs: L.CRS.EPSG3857,
                layers: [this.baseLayers.Google],
                zoomControl: false
            });

            this.map.on('moveend', (e) => {
                var bounds = this.map.getBounds();
                Cookies.set('map-bounds', JSON.stringify(bounds), { expires: 365 });

                if (this.options.centerChanged) {
                    this.options.centerChanged(this.map.getCenter());
                }
            });
        },

        onShow: function() {
            this.map.invalidateSize(false);

            // restore initial bounds
            if (this.initialBounds) {
                this.map.fitBounds(this.initialBounds);
            }

            if (!this.map.searchControl) {
                this.addSearchControl();
            }

            if (!this.map.layersControl) {
                this.addLayersControl();
            }

            if (!this.map.zoomControl) {
                this.addZoomControl();
            }
        },

        /*
        setLayerVisible(name, visible) {
            var self = this;

            var userLayer = self.options.userLayers[name];
            if (!userLayer) {
                console.log('layer not found: ' + name);
                return;
            }

            if (visible) {
                self.map.addLayer(userLayer.layer);
            } else {
                self.map.removeLayer(userLayer.layer);
            }
        }
        */

        addSearchControl() {
            var GoogleSearch = L.Control.extend({
                options: {
                    position: 'topright',
                },
                onAdd: function() {
                    var element = document.createElement("input");
                    element.id = "searchBox";
                    element.placeholder="Поиск по карте"
                    return element;
                }
            });

            (new GoogleSearch).addTo(this.map);

            var input = document.getElementById('searchBox');

            this.map.searchControl = new google.maps.places.SearchBox(input);

            this.map.searchControl.addListener('places_changed', () => {
                var places = this.map.searchControl.getPlaces();

                if (places.length == 0) {
                    return;
                }

                var group = L.featureGroup();

                places.forEach(function(place) {

                    var marker = L.marker([
                        place.geometry.location.lat(),
                        place.geometry.location.lng()
                    ]);

                    group.addLayer(marker);
                });

                group.addTo(this.map);
                this.map.fitBounds(group.getBounds());
            })
        },

        addLayersControl() {
            var groupedOverlays = {
                //'HD снимки': this.layersHd(),
                //'Кадастровая карта': this.layersCadastre(),
                '<div class="header">Мониторинг</div>': this.layersUser(),
                '<div class="header">ВП РФ</div>ЗОНЫ': this.layersVpZones(),
                'АЭРОДРОМЫ И ЛИНИИ': this.layersVpArdLines(),
                //'<div class="header">GeoJSON</div>': this.layersGeojson(),
                '<div class="header">Погода</div>': this.layersWeather()
            };

            this.map.layersControl = L.control.groupedLayers(this.baseLayers, groupedOverlays);
            this.map.layersControl.addTo(this.map);
        },

        addZoomControl() {
            this.map.zoomControl = L.control.zoom({ position:'topright' });
            this.map.zoomControl.addTo(this.map);
        },

        layersVpZones() {
            return {
                'Зоны ограничения полётов': zone('lz'),
                'Запретные зоны': zone('ez'),
                /*'Запретные зоны': new ZoneFeatures({ 
                    layer: 'ez',
                    featureName: 'Запретная зона',
                    style: function(feature) {
                        return {
                            fillColor: '#f00',
                            color: '#f00',
                            weight: 1,
                            opacity: 1,
                            fillOpacity: 0.15
                        }
                    }
                }),*/
                'Опасные зоны': zone('dz'),
            }
        },

        layersVpArdLines: function() {
            return {
                'Аэродромы': new PointFeatures({ 
                    layer: 'ad',
                    featureName: 'Аэропорт',
                    iconUrl: 'img/targets/uav.svg',
                    markerColor: 'rgba(0, 191, 255, 0.6)'
                }),
                'Вертодромы': new PointFeatures({ 
                    layer: 'vd',
                    featureName: 'Вертодром',
                    iconUrl: 'img/targets/heli.svg',
                    markerColor: 'rgba(34, 139, 34, 0.6)'
                })
                /*'Аэродромы': zone('ard'),
                'Воздушные линии': zone('vl'),
                'МВЛ': zone('mvl')
                */
            }
        },

        layersHd() {
            const agricultureOpacity = 0.6;

            var overlayVisual = new L.TileLayer(
                'https://tms.okocenter.ru/data/visual/gmt/2016-06-05/{z}/{x}/{y}.png', {
                    opacity: agricultureOpacity
                }
            );

            var overlayNdvi = this.options.ndviBoundary && this.options.ndviBoundary.coordinates.length > 0
                ? new L.TileLayer.BoundaryCanvas(
                    'https://tms.okocenter.ru/data/ndvi/gmt/2016-06-05/{z}/{x}/{y}.png', {
                    opacity: agricultureOpacity,
                    boundary: this.options.ndviBoundary
                })
                : new L.TileLayer(
                    'https://tms.okocenter.ru/data/ndvi/gmt/2016-06-05/{z}/{x}/{y}.png', {
                    opacity: agricultureOpacity
                });

            return {
                'Visual': overlayVisual,
                'NDVI': this.overlayNdvi
            }
        },

        layersCadastre() {
            /*
            var overlayCadastre = L.tileLayer.Rosreestr(
                'http://{s}.pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/export?dpi=96&transparent=true&format=png32&bbox={bbox}&size=256,256&bboxSR=102100&imageSR=102100&f=image', {
                    tileSize: 256,
                    clickable: true,
                    attribution: 'Росреестр'
                }
            )

            return {
                'Россия': overlayCadastre
            }
            */
        },

        layersUser() {
            _.values(this.options.userLayers).forEach((userLayer) => {
                if (userLayer.show) {
                    this.map.addLayer(userLayer.layer);
                }
            });

            return _.mapObject(this.options.userLayers, (value, key) => {
                return value.layer;
            })
        },
        
        layersWeather() {
            var optionsOWM = { 
                opacity: 0.5,
                showLegend: true, 
                legendPosition: 'bottomright',
                appId: 'e220da0ef978e0dea95beac16028ca30'
            };

            return {
                'Облачность' : L.OWM.clouds(optionsOWM),
                'Осадки' : L.OWM.precipitation(optionsOWM),
                'Давление' : L.OWM.pressure(optionsOWM),
                'Ветер' : L.OWM.wind(optionsOWM),
                'Температура' : L.OWM.temperature(optionsOWM)
            }
        },

        layersGeojson() {
            return {
                //'Опасные зоны': new LayerZoneFeatures({ layer: 'dz' }),
                /*'Аэродромы': new PointFeatures({ 
                    layer: 'ad',
                    iconUrl: 'img/targets/uav.svg'
                }),
                'Вертодромы': new PointFeatures({ 
                    layer: 'vd',
                    iconUrl: 'img/targets/heli.svg'
                })*/
            }
        }
        
        /*
        layersGeojson() {
            var geojsonLayer = new L.GeoJSON(null, {
                style: function(feature) {
                    return {
                        fillColor: Ndvi.ndviColor(feature.properties['NVDI']),
                        color: '#333',
                        weight: 1,
                        opacity: 1,
                        fillOpacity: 1
                    }
                },
                onEachFeature: function(feature, layer) {
                    if (feature.properties) {

                        var popupString = '<div class="popup">';
                        for (var k in feature.properties) {
                            var v = feature.properties[k];
                            popupString += k + ': ' + v + '<br />';
                        }
                        popupString += '</div>';
                        layer.bindPopup(popupString, {
                            maxHeight: 200
                        });
                    }
                }
            });

            return {
                'geojson': geojsonLayer
            }
        }*/
    });
});