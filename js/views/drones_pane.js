define([
    'jquery',
    'underscore',
    'marionette',
    'views/drone_item',
    
    'tpl!templates/drones_pane.tmpl'
    
], function($, _, Marionette, ItemView, templatePanel) {

    return Marionette.CompositeView.extend({
        template: templatePanel,
        itemView: ItemView,
        itemViewContainer: '.drones',
        tagName: 'div',
        className: "drones-pane",

        ui: {
            filter: 'input[name="filter"]'
        },

        events: {
            'keyup input[name="filter"]': 'filterChanged',
            'click .remove': 'filterRemoved'
        },

        templateHelpers: {
            editable: function() {
                return app.account.get('_id');
            }
        },

        filterChanged: function() {
            var name = this.ui.filter.val();

            this.collection.removeFilter('name');

            if (!name) {
                return;
            }

            this.collection.filterBy('name', function(model) {
                return model.get('model').indexOf(name) >= 0;
            });
        },

        filterRemoved() {
            this.ui.filter.val('');
            this.filterChanged();
        }
    });
});
