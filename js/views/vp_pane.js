define([
    'jquery',
    'underscore',
    'marionette',

    'tpl!templates/vp_pane.tmpl',

    'jquery.serializeobject',

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'vp-pane',

        ui: {
            'alert': '.alert'
        },

        events: {
            'click .btn-import': 'handleImport'
        },

        templateHelpers: {
        },

        onRender: function() {
            var self = this;
        },

        handleImport: function() {
            var self = this;
            
            var data = $(self.el).find('form').serializeObject();

            self.ui.alert.hide();
            
            var options = {
                type: 'POST',
                wait: true,
                patch: true,
                success: function (model, response) {
                    //app.navigate('/users/' + model.get('_id'), { trigger: true });
                },
                error: function (model, response) {
                    self.ui.alert.text(response.responseJSON 
                        ? response.responseJSON.error
                        : response.responseText);

                    self.ui.alert.show();
                    self.ui.alert.focus();
                }
            }
        }
    })
});
