define([
    'underscore',
    'marionette',
    
    'tpl!templates/drone_item.tmpl',
    
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'drone-item'
    });
});