define([
    'underscore',
    'marionette',

    'widgets/targets/targets_widget',

    'models/target',
    'collections/track',

    'layers/playback_track',
    'layers/playback_target',

    'tpl!templates/sidebar_widget.tmpl',

    'bootstrap-multiselect',
    'bootstrap-datetimepicker',
    'leaflet-sidebar',
    'css!bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
    'css!bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'

], function(_, Marionette,

    TargetsWidget,

    Target,
    Track,
    PlaybackTrack,
    PlaybackTarget,

    template) {

    function multiselectData(entities) {
        var data = [];

        data.push({
            label: 'Выберите',
            value: ''
        });

        entities.forEach(function(entity) {
            data.push({
                label: entity.id,
                value: entity.id
            });
        });

        return data;
    }

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        id: 'sidebar',
        className: 'sidebar collapsed',

        ui: {
            'targetsWidgetWrapper': '.targets-widget-wrapper',
            'select': '.multiselect',
            'timeFrom': 'input[name="time-from"]',
            'timeTo': 'input[name="time-to"]',
            'load': '.load',
            'unload': '.unload',
            'factor': 'input[name="factor"]',
            
            'play': '.play',
            'step': '.step',
            'stop': '.stop',

            'time': '.time',
            'show': '.show'
        },

        events: {
            'click .load': 'onTrackLoad',
            'click .unload': 'onTrackUnload',
            
            'click .play': 'onPlaybackReplay',
            'click .step': 'onPlaybackStep',
            'click .stop': 'onPlaybackStop',
            
            'click .show': 'onPlaybackShow',

            'click .input-group-addon': 'onClickDatetimeCurrent'
        },

        modelEvents: {},

        templateHelpers: {},

        initialize: function(options) {
            var self = this;

            self.options = options || {};
        },

        onRender: function() {
            var self = this;

            self.initTargets();
            self.initPlayback();
            self.initPlaybackTargetSelector();
            self.initPlaybackDatePicker();
        },

        initTargets: function() {
            var self = this;

            self.targetsWidget = new TargetsWidget({
                collection: self.collection
            });

            self.ui.targetsWidgetWrapper.html(self.targetsWidget.render().el);
        },

        initPlayback: function() {
            var self = this;

            self.track = new Track;

            self.options.mapWidget.map.addLayer(new PlaybackTrack({
                collection: self.track
            }));

            self.options.mapWidget.map.addLayer(new PlaybackTarget({
                collection: self.track,
                icons: {
                    uav: new L.icon({
                        iconUrl: 'img/targets/uav-green.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    }),
                    boat: new L.icon({
                        iconUrl: 'img/targets/boat-green.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    }),
                    man: new L.icon({
                        iconUrl: 'img/targets/man-green.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    }),
                    vehicle: new L.icon({
                        iconUrl: 'img/targets/vehicle-green.svg',
                        iconSize: [36, 36],
                        iconAnchor: [18, 18],
                        popupAnchor: [0, -25],
                        shadowUrl: null
                    })
                }
            }));

            self.track.on('reset', self.onTrackReset.bind(self));
        },

        initPlaybackTargetSelector: function() {
            var self = this;

            self.ui.select.multiselect({
                onChange: function(option, checked, select) {
                    var targetId = $(option).val();
                    self.setTrackLoadControlsDisabled(!targetId);

                    var target = self.collection.get(targetId);
                    if (target) {
                        target.trigger('selected', target);
                    }
                }
            })

            self.setTrackSettingsControlsDisabled(false);
            self.setTrackLoadControlsDisabled(true);
            self.setTrackUnloadControlsDisabled(true);
            self.setTrackPlayControlsDisabled(true);
            self.setTrackProgressControlsDisabled(true);

            self.collection.getRegisteredTargets(function(err, targets) {
                self.ui.select.multiselect('dataprovider', multiselectData(targets));
            })
        },

        setTrackSettingsControlsDisabled: function(val) {
            var self = this;

            $(self.el).find('.dropdown-toggle').prop('disabled', val);
            self.ui.timeFrom.prop('disabled', val);
            self.ui.timeTo.prop('disabled', val);
        },

        setTrackLoadControlsDisabled: function(val) {
            var self = this;

            self.ui.load.prop('disabled', val);
        },

        setTrackUnloadControlsDisabled: function(val) {
            var self = this;

            self.ui.unload.prop('disabled', val);
        },

        setTrackPlayControlsDisabled: function(val) {
            var self = this;

            self.ui.play.prop('disabled', val);
            self.ui.step.prop('disabled', val);
            self.ui.stop.prop('disabled', val);
        },

        setTrackProgressControlsDisabled: function(val) {
            var self = this;

            self.ui.time.text('00:00:00');
            self.ui.show.prop('disabled', val);
        },

        initPlaybackDatePicker: function() {
            var self = this;

            var today = new Date();

            var yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1);
            yesterday.setHours(0,0,0,0);

            var hourago = new Date(today.getTime() - (1000 * 60 * 60));

            self.ui.timeFrom.datetimepicker({
                locale: 'ru',
                defaultDate: hourago
            });

            self.ui.timeTo.datetimepicker({
                locale: 'ru',
                defaultDate: today
            });
        },

        onTrackLoad: function() {
            var self = this;

            var targetId = self.ui.select.val();
            if (!targetId) {
                return;
            }

            var timeFrom = self.ui.timeFrom.data('DateTimePicker').date().unix(); // utc sec
            var timeTo = self.ui.timeTo.data('DateTimePicker').date().unix(); // utc sec
            

            self.setTrackLoadControlsDisabled(true);

            self.track.fetch({
                targetId: targetId,
                from: timeFrom,
                to: timeTo,
                reset: true,
                success: function() {
                }
            })
        },

        onTrackUnload: function() {
            var self = this;

            self.track.reset();

            self.onPlaybackStop();
        },

        onClickDatetimeCurrent: function(e) {
            var self = this;

            if ($(e.target).hasClass('from')) {
                self.ui.timeFrom.data('DateTimePicker').date(new Date());
            } else {
                self.ui.timeTo.data('DateTimePicker').date(new Date());
            }
        },

        onTrackReset: function() {
            var self = this;

            self.setTrackPlayControlsDisabled(this.track.length === 0);
            self.setTrackSettingsControlsDisabled(this.track.length > 0);

            self.setTrackLoadControlsDisabled(this.track.length > 0);
            self.setTrackUnloadControlsDisabled(this.track.length === 0);
        },

        onPlaybackReplay: function(e) {
            var self = this;

            if (self.playing) {
                self.onPlaybackPause();
                return;
            }

            self.playing = true;

            var targetId = self.ui.select.val();
            var target = new Target({
                id: targetId,
                valid: true
            });

            target.on('change', self.onTargetUpdated.bind(self));

            var timeFactor = self.ui.factor.val();

            target.fetch({
                success: function() {
                    self.track.trigger('play', target, timeFactor);
                    self.options.mapWidget.setLayerVisible('Цели (OkoCenter API)', false);

                    self.setTrackProgressControlsDisabled(false);
                }
            });
        },

        onPlaybackPause: function() {
            var self = this;
            
            var timeFactor = self.ui.factor.val();

            self.track.trigger('pause', timeFactor);
        },

        onPlaybackStep: function(e) {
            var self = this;
            
            var backward = $(e.target).hasClass('fa-step-backward');

            self.track.trigger('step', backward);
        },

        onPlaybackStop: function() {
            var self = this;
            
            self.playing = false;

            self.track.trigger('stop');

            self.options.mapWidget.setLayerVisible('Цели (OkoCenter API)', true);
            self.setTrackProgressControlsDisabled(true);
        },

        onPlaybackShow: function() {
            var self = this;
            
            self.track.trigger('show');
        },

        onTargetUpdated: function(model) {
            var self = this;

            if (model.get('valid')) {
                var date = new Date(model.get('position').utc);
                self.ui.time.text(date.toLocaleTimeString());
            }
        }
    });
});