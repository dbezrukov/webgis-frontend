define([
    'underscore',
    'marionette',
    'tpl!templates/target_telemetry_pane.tmpl',

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'target-telemetry-pane unselectable',

        ui: {
        },
        
        events: {
        },
        
        modelEvents: {
            'change': 'render'
        },

        templateHelpers: {
            helperLat: function() {
                return parseFloat(this.position.lat).toFixed(6);
            },
            helperLon: function() {
                return parseFloat(this.position.lon).toFixed(6);
            }
        },

        onRender: function() {
            var self = this;
        }
    });
});
