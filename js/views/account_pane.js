define([
    'jquery',
    'underscore',
    'marionette',

    'models/user',
    'views/flight_request_modal',

    'tpl!templates/account_pane.tmpl',

    'jquery.serializeobject',

], function($, _, Marionette, User, FlightRequestModal, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'account-pane',

        ui: {
            'alert': '.alert'
        },

        events: {
            //'click a.save': 'saveUser'
            'click a.flight-request': 'handleFlightRequest',
            'click a.drone-add': 'handleDroneAdd'
        },

        templateHelpers: {
        },

        onRender: function() {
            var self = this;
        },

        /*
        saveUser: function() {
            var self = this;
            
            var data = $(self.el).find('form').serializeObject();

            self.ui.alert.hide();
            
            var options = {
                type: 'POST',
                wait: true,
                patch: true,
                success: function (model, response) {
                    app.navigate('/users/' + model.get('_id'), { trigger: true });
                },
                error: function (model, response) {
                    self.ui.alert.text(response.responseJSON 
                        ? response.responseJSON.error
                        : response.responseText);

                    self.ui.alert.show();
                    self.ui.alert.focus();
                }
            }
            
            if (self.options.collection) {
                self.options.collection.create(data, options)
            } else {
                self.model.save(data, options);
            }
        },
        */

        handleFlightRequest: function() {
            var flightRequest = new FlightRequestModal({});
            app.layout.modal.show(flightRequest); 
        },

        handleDroneAdd: function() {
            //var flightRequest = new FlightRequestModal({});
            //app.layout.modal.show(flightRequest); 
        }
    })
});
