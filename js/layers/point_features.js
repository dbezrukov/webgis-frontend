define([
    'underscore',
    'marionette',
    'leaflet.markercluster',
    'css!bower_components/leaflet.markercluster/dist/MarkerCluster.css',
    'css!bower_components/leaflet.markercluster/dist/MarkerCluster.Default.css'
    
], function(_) {

    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;
            this.markers = L.markerClusterGroup({
                iconCreateFunction: cluster => {
                    var childCount = cluster.getChildCount();
                    const html = `<div style="background-color: ${this.options.markerColor};"><img class="svg" src="${this.options.iconUrl}" alt=""/><span>${childCount}</span></div>`;
                    return new L.DivIcon({ html: html, className: 'map-marker map-marker-cluster', iconSize: new L.Point(40, 40) });
                }
            });

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: async function(map) {
            
            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!this.initialized) {
                this._map.on('moveend', this.updateBounds.bind(this));
                this.updateBounds();

                this.initialized = true;
            }

            let response = await fetch(`/api/features/${this.options.layer}`);
            let json = await response.json();

            let count = json.features.length;
            console.log(count);

            var geoJsonLayer = L.geoJson(json.features, {
                
                pointToLayer: (feature, latlng) => {
                    const html = `<div style="background-color: ${this.options.markerColor};"><img class="svg" src="${this.options.iconUrl}" alt=""/></div>`;
                    return L.marker(latlng, { 
                        icon: new L.DivIcon({ html: html, className: 'map-marker map-marker-single', iconSize: new L.Point(40, 40) })
                    });
                },
                onEachFeature: (feature, layer) => {
                    if (feature.properties.description) {
                        const header = `${this.options.featureName}: <b>${feature.properties.name}</b><br><br>`;
                        layer.bindPopup(header + feature.properties.description.replace(/\n/g, '<br />'));
                    }
                }
            });
            this.markers.addLayer(geoJsonLayer);


            this.addLayer(this.markers)
        },

        onRemove: function(map) {
            this.markers.clearLayers();
            this.removeLayer(this.markers);

            L.LayerGroup.prototype.onRemove.call(this, map);
        },

        updateBounds: function() {
            if (!this._map) {
                return;
            }

            var bounds = this._map.getBounds();
            this.bounds = {
                south: bounds.getSouthWest().lat,
                west: bounds.getSouthWest().lng,
                north: bounds.getNorthEast().lat,
                east: bounds.getNorthEast().lng
            };
        }
    });
});
