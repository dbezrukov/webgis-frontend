define([
    'underscore',
    'marionette'
    
], function(_) {

    // Compute a polygon "center", use your favorite algorithm (centroid, etc.)
    L.Polygon.addInitHook(function () {
        this._latlng = this._bounds.getCenter();
    });
    
    // Provide getLatLng and setLatLng methods for Leaflet.markercluster to be able to cluster polygons.
    L.Polygon.include({
        getLatLng: function () {
            return this._latlng;
        },
        setLatLng: function () {} // Dummy method.
    });

    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;
            
            this.zones = L.geoJson({
                /*pointToLayer: (feature, latlng) => {
                    const html = `<div style="background-color: ${this.options.markerColor};"><img class="svg" src="${this.options.iconUrl}" alt=""/></div>`;
                    return L.marker(latlng, { 
                        icon: new L.DivIcon({ html: html, className: 'map-marker map-marker-single', iconSize: new L.Point(40, 40) })
                    });
                },*/
                style: options.style,
                onEachFeature: (feature, layer) => {
                    if (feature.properties.description) {
                        const header = `${this.options.featureName}: <b>${feature.properties.name}</b><br><br>`;
                        layer.bindPopup(header + feature.properties.description.replace(/\n/g, '<br />'));
                    }
                }
            });

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: async function(map) {
            
            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!this.initialized) {
                this._map.on('moveend', this.updateBounds.bind(this));
                this.updateBounds();

                this.initialized = true;
            }

            let response = await fetch(`/api/features/${this.options.layer}`);
            let json = await response.json();

            let count = json.features.length;
            console.log(count);

            this.addLayer(this.zones);
        },

        onRemove: function(map) {
            this.zones.clearLayers();
            this.removeLayer(this.zones);

            L.LayerGroup.prototype.onRemove.call(this, map);
        },

        updateBounds: function() {
            if (!this._map) {
                return;
            }

            var bounds = this._map.getBounds();
            this.bounds = {
                south: bounds.getSouthWest().lat,
                west: bounds.getSouthWest().lng,
                north: bounds.getNorthEast().lat,
                east: bounds.getNorthEast().lng
            };
        }
    });
});

/*
define([
    'underscore',
    'marionette',
    
], function(_) {

    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: function(map) {
            var self = this;

            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!self.initialized) {
                self._map.on('moveend', self.updateBounds.bind(self));
                self.options.collection.on('add', self.entityAdded.bind(self));
                self.options.collection.on('remove', self.entityRemoved.bind(self));

                self.updateBounds();

                self.initialized = true;
            }
        },

        onRemove: function(map) {
            var self = this;

            var model;

            while (model = self.options.collection.first()) {
                self.options.collection.remove(model);
            }

            L.LayerGroup.prototype.onRemove.call(self, map);
        },

        updateBounds: function() {
            var self = this;

            if (!self._map) {
                return;
            }

            var bounds = self._map.getBounds();
            self.options.collection.bounds = {
                south: bounds.getSouthWest().lat,
                west: bounds.getSouthWest().lng,
                north: bounds.getNorthEast().lat,
                east: bounds.getNorthEast().lng
            };

            self.options.collection.fetch();
        },

        entityAdded: function(model, collection) {
            var self = this;

            model.feature = L.geoJSON(model.toJSON(), {
                style: self.style
            }).addTo(self);
        },

        entityRemoved: function(model, collection) {
            var self = this;

            if (model.feature) {
                self.removeLayer(model.feature);
            }
        },

        style: function(feature) {
            return {
                fillColor: '#f00',
                color: '#f00',
                weight: 1,
                opacity: 1,
                fillOpacity: 0.15
            }
        }
    });
});
*/