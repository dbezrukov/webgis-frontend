define([
    'underscore',
    'marionette',
    'views/target_telemetry_pane',
    'leaflet-rotatedmarker'

], function(_,
    Marionette,
    TargetTelemetryPane) {

    // Extending Group layer
    // http://leafletjs.com/examples/extending/extending-2-layers.html
    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: function(map) {
            var self = this;

            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!self.initialized) {
                self.initialized = true;

                self.options.collection.on('play', function(model, timeFactor) {
                    console.log('playback_target: play');

                    self.model = model;

                    var position = self.options.collection.first();
                    var pos = L.latLng(position.get('lat'), position.get('lon'));

                    if (self.marker) {
                        self._map.removeLayer(self.marker);
                        delete self.marker
                    }

                    self.marker = L.marker(pos, {
                            icon: self.options.icons[self.model.get('type')]
                        })
                        .bindPopup()
                        .bindTooltip(self.model.get('id'), {
                            noHide: true
                        })
                        .addTo(self);

                    if (model.get('type') === 'uav') {
                        self.marker.setRotationOrigin('center center');
                        self.marker.setRotationAngle(position.get('hdg') - 90.0);
                    }

                    // make content async
                    self.marker.on('popupopen', function(e) {

                        var TargetPaneLayout = Marionette.Layout.extend({
                            template: _.template('<div id="telemetry"></div>'),
                            regions: {
                                telemetry: "#telemetry",
                            }
                        });

                        var layout = new TargetPaneLayout();
                        layout.render();

                        layout.telemetry.show(new TargetTelemetryPane({
                            model: self.model
                        }));

                        e.popup.setContent(layout.el);
                    })

                    self.model.on('change', self.entityChanged.bind(self));

                    self.index = 0;

                    if (self.timer) {
                        clearInterval(self.timer);
                        delete self.timer;
                    }

                    self.timer = setInterval(self.gotoNextPosition.bind(self), 1000 / timeFactor);
                });

                self.options.collection.on('step', function(backward) {
                    console.log('playback_target: step');
                    console.log('backward: ' + backward);

                    if (self.timer) {
                        clearInterval(self.timer);
                        delete self.timer;
                    }

                    if (backward) {
                        self.gotoPrevPosition.call(self);
                    } else {
                        self.gotoNextPosition.call(self);
                    }
                });

                self.options.collection.on('pause', function(timeFactor) {
                    console.log('playback_target: pause');

                    if (self.timer) {
                        clearInterval(self.timer);
                        delete self.timer;
                    } else {
                        self.timer = setInterval(self.gotoNextPosition.bind(self), 1000 / timeFactor);
                    }
                });

                self.options.collection.on('stop', function() {
                    console.log('playback_target: stop');

                    clearInterval(self.timer);

                    if (self.marker) {
                        self._map.removeLayer(self.marker);
                        delete self.marker
                    }
                });

                self.options.collection.on('show', function() {
                    console.log('playback_target: show');

                    var pos = L.latLng(self.model.get('position').lat, self.model.get('position').lon);
                    self._map.panTo(pos);
                });
            }
        },

        gotoPrevPosition: function() {
            var self = this;

            --self.index;

            var position = self.options.collection.at(self.index);
            if (!position) {
                clearInterval(self.timer);
                delete self.timer;
                return;
            }

            self.model.set({
                valid: true,
                position: position.toJSON()
            })
        },

        gotoNextPosition: function() {
            var self = this;

            var position = self.options.collection.at(self.index);
            if (!position) {
                clearInterval(self.timer);
                delete self.timer;
                return;
            }

            self.model.set({
                valid: true,
                position: position.toJSON()
            })

            self.index++;
        },

        entityChanged: function(model) {
            var self = this;

            var pos = L.latLng(model.get('position').lat, model.get('position').lon);
            self.marker.setLatLng(pos);

            if (model.get('type') === 'uav') {
                self.marker.setRotationAngle(model.get('position').hdg - 90.0);
            }
        }
    });
});