define([
    'underscore',
    'marionette',
    
    'views/target_pane',
    'views/target_telemetry_pane',
    'views/target_video_pane',

    'pallete',
    'leaflet-rotatedmarker'

], function(_,
    Marionette,
    
    TargetPane,
    TargetTelemetryPane,
    TargetVideoPane,

    pallete) {

    // Extending Group layer
    // http://leafletjs.com/examples/extending/extending-2-layers.html
    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: function(map) {
            var self = this;

            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!self.initialized) {
                self._map.on('moveend', self.updateBounds.bind(self));
                self.options.collection.on('add', self.entityAdded.bind(self));
                self.options.collection.on('remove', self.entityRemoved.bind(self));
                self.options.collection.on('change', self.entityChanged.bind(self));
                self.options.collection.on('selected', self.entitySelected.bind(self));

                self.updateBounds();

                self.initialized = true;
            }

            self.options.collection.startMonitoring();
        },

        onRemove: function(map) {
            var self = this;

            self.options.collection.stopMonitoring();

            var model;

            while (model = self.options.collection.first()) {
                self.options.collection.remove(model);
            }

            L.LayerGroup.prototype.onRemove.call(self, map);
        },

        updateBounds: function() {
            var self = this;

            if (!self._map) {
                return;
            }

            var bounds = self._map.getBounds();
            self.options.collection.bounds = {
                south: bounds.getSouthWest().lat,
                west: bounds.getSouthWest().lng,
                north: bounds.getNorthEast().lat,
                east: bounds.getNorthEast().lng
            };
        },

        entityAdded: function(model, collection) {
            var self = this;
            var pos = L.latLng(model.get('position').lat, model.get('position').lon);

            model.marker = L.marker(pos, {
                    icon: self.options.icons[model.get('type')]
                })
                .bindPopup()
                .bindTooltip(model.get('id'), {
                    noHide: true
                })
                .addTo(self);

            if (model.get('type') === 'uav' || model.get('type') === 'heli') {
                model.marker.setRotationOrigin('center center');
                model.marker.setRotationAngle(model.get('position').hdg - 90.0);
            }

            model.marker.on('popupopen', function(e) {

                var TargetPaneLayout = Marionette.Layout.extend({
                    template: _.template('\
                        № <b><%= id %></b><br>\
                        <div id="telemetry"></div>\
                        <div id="video" style="width: 160px; margin-top: 10px; margin-bottom: 10px; "></div>\
                        <a class="target-link" style="cursor: pointer;">открыть окно цели</a>\
                        <div id="info"></div>\
                    '),
                    regions: {
                        telemetry: "#telemetry",
                        video: "#video",
                        info: "#info"
                    },

                    events: {
                        'click .target-link': 'onTargetLink'
                    },

                    onTargetLink: function() {
                        var self = this;

                        app.layout.modal.show(new TargetPane({
                            model: self.model
                        }));
                    }
                });

                model.marker.layout = new TargetPaneLayout({
                    model: model // target model
                });
                model.marker.layout.render();

                /* Do not show telemetry here
                model.marker.layout.telemetry.show(new TargetTelemetryPane({
                    model: model // target model
                }));
                */

                model.marker.layout.video.show(new TargetVideoPane({
                    model: model // target model
                }));

                e.popup.setContent(model.marker.layout.el);
            })

            model.marker.on('popupclose', function(e) {
                model.marker.layout.close();
            })

            model.track = new L.Polyline([], {
                color: pallete.getColor(model.get('id')),
                weight: 3,
                opacity: 0.9
            })

            model.track.addTo(self);

            self.maybeHighlightTarget(model);
        },

        maybeHighlightTarget: function(model) {
            var targetId = getQueryVariable('target');
            if (targetId && targetId === model.get('id')) {
                var desirableSource = getQueryVariable('channel');
                if (desirableSource) {
                    model.set('source', new Backbone.Model({
                        name: desirableSource
                    }));
                }

                model.marker.openPopup();
            }

            function getQueryVariable(variable) {
               var query = window.location.search.substring(1);
               var vars = query.split("&");
               for (var i=0;i<vars.length;i++) {
                       var pair = vars[i].split("=");
                       if(pair[0] == variable){return pair[1];}
               }
               return;
            }
        },

        entityRemoved: function(model, collection) {
            var self = this;

            if (model.marker) {
                self.removeLayer(model.marker);
            }

            if (model.track) {
                self.removeLayer(model.track);
            }
        },

        entityChanged: function(model) {
            var pos = L.latLng(model.get('position').lat, model.get('position').lon);
            model.marker.setLatLng(pos);

            model.track.addLatLng(pos);

            if (model.track.getLatLngs().length > 500) {
                model.track.spliceLatLngs(0, 1);
            }

            if (model.get('type') === 'uav' || model.get('type') === 'heli') {
                model.marker.setRotationAngle(model.get('position').hdg - 90.0);
            }
        },

        entitySelected: function(model) {
            var self = this;

            if (!self._map) {
                return;
            }

            var pos = L.latLng(model.get('position').lat, model.get('position').lon);
            self._map.panTo(pos);
        }
    });
});
