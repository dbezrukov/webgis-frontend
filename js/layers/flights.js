define([
    'underscore',
    'sprintfjs',
    'leaflet-rotatedmarker'

], function(_, sprintf) {

    // Extending Group layer
    // http://leafletjs.com/examples/extending/extending-2-layers.html
    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: function(map) {
            var self = this;

            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!self.initialized) {
                self._map.on('moveend', self.updateBounds.bind(self));
                self.options.collection.on('add', self.entityAdded.bind(self));
                self.options.collection.on('remove', self.entityRemoved.bind(self));
                self.options.collection.on('change', self.entityChanged.bind(self));

                self.updateBounds();

                self.initialized = true;
            }

            self.options.collection.startMonitoring();
        },

        onRemove: function(map) {
            var self = this;

            self.options.collection.stopMonitoring();

            var model;

            while (model = self.options.collection.first()) {
                self.options.collection.remove(model);
            }

            L.LayerGroup.prototype.onRemove.call(self, map);
        },

        updateBounds: function() {
            var self = this;

            if (!self._map) {
                return;
            }
            var bounds = self._map.getBounds();
            self.options.collection.bounds = {
                south: bounds.getSouthWest().lat,
                west: bounds.getSouthWest().lng,
                north: bounds.getNorthEast().lat,
                east: bounds.getNorthEast().lng
            };
        },

        entityAdded: function(model, collection) {
            var self = this;

            var titleTemplate = 'Рейс: <b>%s</b><br>Откуда: <b>%s</b><br>Куда: <b>%s</b><br>Самолет: <b>%s</b>';

            var title = sprintf.sprintf(titleTemplate,
                model.get('Call') || 'н/д',
                model.get('From') || 'н/д',
                model.get('To') || 'н/д',
                model.get('Mdl') || 'н/д'
            );

            var pos = L.latLng(model.get('Lat'), model.get('Long'));

            var icon = new L.icon({
                iconUrl: 'img/targets/plane-yellow.svg',
                iconSize: [36, 36],
                iconAnchor: [18, 18],
                popupAnchor: [0, -25],
                shadowUrl: null
            });

            model.marker = L.marker(pos, {
                    icon: icon
                })
                .bindPopup()
                .addTo(self);

            model.marker.setRotationOrigin('center center');
            model.marker.setRotationAngle(model.get('Trak') - 90.0);

            // make content async
            model.marker.on('popupopen', function(e) {
                e.popup.setContent(title);
            })
        },

        entityRemoved: function(model, collection) {
            var self = this;

            if (model.marker) {
                self.removeLayer(model.marker);
            }
        },

        entityChanged: function(model) {
            var pos = L.latLng(model.get('Lat'), model.get('Long'));
            model.marker.setLatLng(pos);
            model.marker.setRotationAngle(model.get('Trak') - 90.0);
        },
    });
});