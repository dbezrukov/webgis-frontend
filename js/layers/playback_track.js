define([
    'underscore',
    'leaflet-rotatedmarker'

], function(_) {

    // Extending Group layer
    // http://leafletjs.com/examples/extending/extending-2-layers.html
    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: function(map) {
            var self = this;

            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!self.initialized) {
                self.options.collection.on('reset', self.onTrackReset.bind(self));

                self.initialized = true;

                self.track = new L.Polyline([], {
                    color: 'lightgrey',
                    weight: 3,
                    opacity: 0.9
                })

                self.track.markers = [];
                self.track.addTo(map);
            }
        },

        onTrackReset: function() {
            var self = this;

            console.log('TrackReset: 1 removing drawn track');

            self.track.markers.forEach(function(marker) {
                self.track._map.removeLayer(marker);
            })

            console.log('TrackReset: 2 drawing new one');

            console.log('track length: ' + self.options.collection.length);

            var positions = _.map(self.options.collection.models, function(value) {
                return L.latLng(value.get('lat'), value.get('lon'));
            });

            self.track.setLatLngs(positions);
            self.track.bringToFront();

            if (positions.length < 2) {
                return;
            }

            console.log('TrackReset: 3 add some track markers');

            addTrackMarker(positions[0], stringFromUtc(self.options.collection.first().get('utc')));
            addTrackMarker(positions.slice(-1)[0], stringFromUtc(self.options.collection.last().get('utc')));

            function addTrackMarker(pos, text) {
                if (!pos) {
                    return;
                }

                self.track.markers.push(L.marker(pos, {
                        icon: new L.icon({
                            iconUrl: 'img/1x1.png',
                            iconSize: [20, 20],
                            iconAnchor: [10, 10],
                            shadowUrl: null
                        })
                    })
                    .bindTooltip(text, {
                        noHide: true
                    })
                    .addTo(self.track._map));

                self.track.markers.push(L.circleMarker(pos, {
                        color: 'lightgrey',
                        fillColor: 'lightgrey',
                        fillOpacity: 0.3,
                        radius: 5
                    })
                    .addTo(self.track._map));
            }

            function stringFromUtc(utc) {
                var date = new Date(utc);
                return date.toLocaleTimeString();
            }

            console.log('TrackReset: 4 focusing the track');

            var group = new L.featureGroup(self.track.markers);
            self.track._map.fitBounds(group.getBounds(), {
                padding: [300, 50]
            });
        },

        /*
        onRemove: function(map) {
            var self = this;

            self.options.collection.stopMonitoring();

            var model;

            while (model = self.options.collection.first()) {
                self.options.collection.remove(model);
            }

            L.LayerGroup.prototype.onRemove.call(self, map);
        },

        entityAdded: function(model, collection) {
            var self = this;
            var pos = L.latLng(model.get('position').lat, model.get('position').lon);

            var options = {}

            var icon = self.options.icons[model.get('type')];
            if (icon) {
                options.icon = icon;
            } else {
                console.log('missing icon for type: ' + model.get('type'));
            }

            var color = pallete.getColor(model.get('id'));

            model.marker = L.marker(pos, options)
                .bindPopup()
                .bindLabel(model.get('id'), {
                    noHide: true,
                    offset: [-20, 17],
                    className: 'leaflet-marker-label'
                })
                .addTo(self);

            if (model.get('type') === 'uav') {
                model.marker.setRotationOrigin('center center');
                model.marker.setRotationAngle(model.get('position').hdg - 90.0);
            }

            model.track = new L.Polyline([], {
                color: #777,
                weight: 3,
                opacity: 0.9
            })

            model.track.addTo(self);
        },

        entityRemoved: function(model, collection) {
            var self = this;

            if (model.marker) {
                self.removeLayer(model.marker);
            }
        },

        entityChanged: function(model) {
            var pos = L.latLng(model.get('position').lat, model.get('position').lon);
            model.marker.setLatLng(pos);

            model.track.addLatLng(pos);

            if (model.track.getLatLngs().length > 500) {
                model.track.spliceLatLngs(0, 1);
            }

            if (model.get('type') === 'uav') {
                model.marker.setRotationAngle(model.get('position').hdg - 90.0);
            }
        },
        */
    });
});