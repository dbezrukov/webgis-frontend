define([

    'jquery',
    'underscore',
    'marionette',

    'application/application',

    'views/auth/login_modal',
    'widgets/navbar/navbar_widget',

    'views/main_pane',
    'views/account_pane'

], function($, _, Marionette, 

	Application,

    LoginModal,
    NavbarWidget, 

	MainPane,
    AccountPane) {

	var app = new Application();
    window.app = app;

    app.on("initialize:after", function() {

	  	console.log('Initilizing WebGIS app');

        $('body').append(app.layout.render().el);
	  	
	  	var modes = [
            '<li>\
                <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="#">Мониторинг</a>\
                <a class="hidden-xs" href="#">Мониторинг</a>\
            </li>',
            '<li>\
                <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/about">О проекте</a>\
                <a class="hidden-xs" href="/about">О проекте</a>\
            </li>',
            /*'<li>\
                <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/forum">Форум</a>\
                <a class="hidden-xs" href="/forum">Форум</a>\
            </li>'*/
	  	]

        if (app.account.get('_id')) {
            modes.splice(1, 0, ...[
                '<li>\
                    <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/drones">Мои БПЛА</a>\
                    <a class="hidden-xs" href="/drones">Мои БПЛА</a>\
                </li>',
                '<li>\
                    <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/requests">Заявки</a>\
                    <a class="hidden-xs" href="/requests">Заявки</a>\
                </li>',
                '<li>\
                    <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/account">Профиль</a>\
                    <a class="hidden-xs" href="/account">Профиль</a>\
                </li>'
            ])
        }

        var accountWidget = '';

        if (app.account.get('_id')) {
            
            accountWidget = '<li class="dropdown" style="display: flex; align-items:  center;">\
                <img class="avatar hidden-xs" src="<%= avatar %>"/>\
                <a class="dropdown-toggle" data-toggle="dropdown">\
                    <%= name %> <span class="caret"></span>\
                </a>\
                <ul class="dropdown-menu user-menu">\
                    <li><a href="/admin">Администрирование</a></li>\
                    <li><a href="https://droneradar.org/api" target="_blank">Документация по API</a></li>\
                    <li><a href="/logout">Выйти</a></li>\
                </ul>\
            </li>'
        } else {
            
            accountWidget = '<li><a class="login-button" href="#">Войти</a></li>';
        }

        var widgets = [
            _.template(accountWidget)(_.extend(app.account.toJSON(), {
                avatar: app.account.get('photostorage') + app.account.get('photo')
            }))
        ]

	  	app.layout.navbar.show(new NavbarWidget({
	  		model: app.account,
	  		modes: modes,
	  		widgets: widgets,
            events: {
                'click .login-button': function() {
                    app.layout.modal.show(new LoginModal({
                        model: app.account
                    }));
                }
            },
	  	}));

	  	app.route = new Route();

        Backbone.history.start({
            pushState: true,
            root: '/'
        });
    });

    var Route = Backbone.Marionette.AppRouter.extend({
        routes : {
            '': 'goMain',
            'drones': 'goDrones',
            'drones/create': 'goDroneCreate',
            'drones/:id': 'goDrone',
            'requests': 'goRequests',
            'requests/create': 'goRequestCreate',
            'requests/:id': 'goRequest',
            'account': 'goAccount',
        },
        goMain: function() {
            app.layout.content.show(new MainPane({
                model: app.account
            }));
        },
        goRequests: function() {
            require(['views/requests_pane', 'collections/requests', 'backbone-filtered-collection'], 
                (RequestsPane, Requests, FilteredCollection) => {
                
                var requests = new Requests;

                requests.fetch({
                    success: () => {
                        app.layout.content.show(new RequestsPane({
                             model: new Backbone.Model(),
                            collection: new FilteredCollection(requests)
                        }));
                    }
                });
            })
        },
        goRequestCreate: function() {
            require(['views/request_editor', 'models/request'], (RequestEditor, Request) => {

                var request = new Request();
                
                app.layout.content.show(new RequestEditor({
                    model: request
                }));
            })
        },
        goRequest: function(requestId) {
            require(['views/request_editor', 'models/request'], (RequestEditor, Request) => {
                
                var request = new Request({
                    _id: requestId
                });

                request.fetch({
                    success: () => {
                        app.layout.content.show(new RequestEditor({
                            model: request
                        }));
                    },
                    error: () => {
                        app.navigate('/requests', { trigger: true });
                    }
                });
            })
        },
        goDrones: function() {
            require(['views/drones_pane', 'collections/drones', 'backbone-filtered-collection'], 
                (DronesPane, Drones, FilteredCollection) => {
                
                var drones = new Drones;

                drones.fetch({
                    success: () => {
                        app.layout.content.show(new DronesPane({
                            model: new Backbone.Model(),
                            collection: new FilteredCollection(drones)
                        }));
                    }
                });
            })
        },
        goDroneCreate: function() {
            require(['views/drone_editor', 'models/drone'], (DroneEditor, Drone) => {

                var drone = new Drone();
                
                app.layout.content.show(new DroneEditor({
                    model: drone
                }));
            })
        },
        goDrone: function(droneId) {
            require(['views/drone_editor', 'models/drone'], (DroneEditor, Drone) => {

                var drone = new Drone({
                    _id: droneId
                });

                drone.fetch({
                    success: () => {
                        app.layout.content.show(new DroneEditor({
                            model: drone
                        }));
                    },
                    error: () => {
                        app.navigate('/drones', { trigger: true });
                    }
                });
            })
        },
        goAccount: function() {
            app.layout.content.show(new AccountPane({
                model: app.account
            }));
        }
    });

    return app;
});
