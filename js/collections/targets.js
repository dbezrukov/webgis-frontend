define([
    'underscore',
    'sprintfjs',
    'backbone',
    'models/target'

], function(_, sprintf, Backbone, Target) {

    return Backbone.Collection.extend({
        model: Target,

        url: function() {
            var url = '/api/targets?fNBnd=%f&fEBnd=%f&fSBnd=%f&fWBnd=%f&since=%i';

            return sprintf.sprintf(url,
                this.bounds.north,
                this.bounds.east,
                this.bounds.south,
                this.bounds.west,
                this.since || 0);
        },

        initialize: function() {
            var self = this;
        },

        parse: function(response) {
            this.since = response.stm;
            return response.targets;
        },

        startMonitoring: function() {
            var self = this;

            console.log('TARGETS: startMonitoring');

            self.timer = setInterval(function() {

                self.fetch({
                    error: function() {
                        console.log('TARGETS: request error');

                        self.forEach(function(entity) {
                            self.remove(entity);
                        })
                    },
                    remove: false
                });
            }, 3000);

            self.fetch();
        },

        stopMonitoring: function() {
            var self = this;

            console.log('TARGETS: stopMonitoring');

            if (self.timer) {
                clearInterval(self.timer);
            }
        },

        getRegisteredTargets: function(callback) {
            var self = this;

            $.ajax({
                    type: 'GET',
                    url: '/api/targets/registered',
                    dataType: 'json'
                })
                .done(function(response) {
                    callback(null, response);
                })
                .fail(function(jqXHR, textStatus, err) {
                    callback(JSON.parse(jqXHR.responseText).error);
                });
        },

        bounds: {
            south: 0,
            west: 0,
            north: 0,
            east: 0
        }
    });
});