define([
    'underscore',
    'sprintfjs',
    'backbone',
    'models/feature'

], function(_, sprintf, Backbone, Feature) {

    return Backbone.Collection.extend({
        model: Feature,

        url: function() {
            var url = `/api/features/${this.type}?fNBnd=%f&fEBnd=%f&fSBnd=%f&fWBnd=%f&since=%i`;

            return sprintf.sprintf(url,
                this.bounds.north,
                this.bounds.east,
                this.bounds.south,
                this.bounds.west,
                this.since || 0);
        },

        initialize: function(type) {
            this.type = type;
        },

        parse: function(response) {
            this.since = response.stm;
            return response.features;
        },

        /*
        fetchFeatures: function() {
            var self = this;

            self.fetch({
                error: function() {
                    console.log('FEATURES: request error');

                    self.forEach(function(entity) {
                        self.remove(entity);
                    })
                },
                remove: false
            });
        },
        */

        bounds: {
            south: 0,
            west: 0,
            north: 0,
            east: 0
        }
    });
});