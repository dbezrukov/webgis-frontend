define([
    'backbone',
    'models/source'
    
], function(Backbone, Source) {

    return Backbone.Collection.extend({
        model: Source,
        url: function() {
            return ('/api/targets/' + this.targetId + '/sources');
        },

        initialize: function(options) {
            options || (options = {});
            this.targetId = options.targetId;
        }
    });
});
