define([
    'underscore',
    'sprintfjs',
    'backbone',
    'models/flight'

], function(_, sprintf, Backbone, Flight) {
    return Backbone.Collection.extend({
        model: Flight,
        urlRadarServer: function() {
        	var url = 'https://global.adsbexchange.com/VirtualRadar/AircraftList.json?fNBnd=%f&fEBnd=%f&fSBnd=%f&fWBnd=%f&ldv=%i';
        	
        	return sprintf.sprintf(url,
        		this.bounds.north,
        		this.bounds.east,
        		this.bounds.south,
        		this.bounds.west,
        		this.lastDv || 0);
        },
        url: function() {
        	return this.urlRadarServer();
        },
        initialize: function(){
            var self = this;
        },

        parse: function(response) {
        	this.lastDv = response.lastDv;
    		return response.acList;
  		},
        
        startMonitoring: function() {
        	var self = this;

        	console.log('FLIGHTS: startMonitoring');

        	self.timer = setInterval(function() {
        		
				self.fetch({
					dataType: 'jsonp',
					error: function() {
						console.log('FLIGHTS: request error');

						self.forEach(function(entity) {
							self.remove(entity);
						})
					}
				});
        	}, 2000);

        	self.fetch({
        		dataType: 'jsonp'
        	});
        },

        stopMonitoring: function() {
        	var self = this;

        	console.log('FLIGHTS: stopMonitoring');

        	if (self.timer) {
        		clearInterval(self.timer);
        	}
        },

        bounds: {
        	south: 0,
			west:  0,
            north: 0,
            east:  0
        }
    });
});
