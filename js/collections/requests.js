define([
    'backbone',
    'models/request'

], function(Backbone, ItemModel) {
    
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
            return '/api/flightrequests';
        }
    });
});
