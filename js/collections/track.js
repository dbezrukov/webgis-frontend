define([
    'underscore',
    'sprintfjs',
    'backbone',
    'models/position'

], function(_, sprintf, Backbone, Position) {

    return Backbone.Collection.extend({
        model: Position,

        fetch: function(options) {

            var urlTemplate = '/api/targets/%s/track?from=%i&to=%i';

            options.url = sprintf.sprintf(urlTemplate,
                options.targetId,
                options.from,
                options.to);
            
            return Backbone.Collection.prototype.fetch.call(this, options);
        },


        initialize: function() {
            var self = this;
        },

        nextTarget: function() {

        }

        /*
        parse: function(response) {
            this.since = response.stm;
            return response.targets;
        },*/

        /*
        startMonitoring: function() {
            var self = this;

            console.log('TARGETS: startMonitoring');

            self.timer = setInterval(function() {

                self.fetch({
                    error: function() {
                        console.log('TARGETS: request error');

                        self.forEach(function(entity) {
                            self.remove(entity);
                        })
                    },
                    remove: false
                });
            }, 1000);

            self.fetch();
        },

        stopMonitoring: function() {
            var self = this;

            console.log('TARGETS: stopMonitoring');

            if (self.timer) {
                clearInterval(self.timer);
            }
        },

        getRegisteredTargets: function(callback) {
            var self = this;

            $.ajax({
                    type: 'GET',
                    url: '/api/targets/registered',
                    dataType: 'json'
                })
                .done(function(response) {
                    callback(null, response);
                })
                .fail(function(jqXHR, textStatus, err) {
                    callback(JSON.parse(jqXHR.responseText).error);
                });
        },

        bounds: {
            south: 0,
            west: 0,
            north: 0,
            east: 0
        }
        */
    });
});