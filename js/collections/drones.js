define([
    'backbone',
    'models/drone'

], function(Backbone, ItemModel) {
    
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
            return '/api/drones';
        }
    });
});
